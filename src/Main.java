import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("(1) Golden = Rs.200");
        System.out.println("(2) Silver = Rs.150");
        System.out.print("Please select category of ticket from list above: ");
        Scanner catg = new Scanner(System.in);
        int a = catg.nextInt();
        System.out.print("How many ticket would you like? : ");
        Scanner t = new Scanner(System.in);
        int b = t.nextInt();

        int p = 0;
        if (a == 1) {
            p = b * 200;
        } else if (a == 2) {
            p = b * 150;
        } else {
            System.out.println("Please select correct category of ticket!");
            System.out.println("Thank you.");
        }
        System.out.println("Total: Rs." + p);
    }
}
